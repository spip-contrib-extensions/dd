<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

return [
	'dd_description' => 'Procure une fonction debug() à utiliser en PHP, un filtre |dd et une balise #ENVDD à utiliser dans les squelettes.',
	'dd_nom'         => 'DD',
	'dd_slogan'      => 'Débuggage et barre d\'infos utiles',
];

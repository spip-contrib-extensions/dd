<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

return [

	// C
	'charge_minute'         => 'Charge serveur cette dernière minute (load)',
	'charge_5_minutes'      => 'Charge serveur des 5 dernières minutes (load)',
	'charge_15_minutes'     => 'Charge serveur des 15 dernières minutes (load)',

	// D
	'dd_titre'              => 'Dd',

	// M
	'memoire_utilisee'      => 'Mémoire utilisée',

	// N
	'nombre_requetes_mysql' => 'Nombre de requêtes Mysql exécutées',

	// T
	'temps_execution_page'  => 'Temps d\'exécution de la page',

];

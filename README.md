# **DD**, dump et debug de base

1/ Ajoute une balise`#ENVDD`permettant de dumper tout l'#ENV, ou bien certaines valeurs uniquement avec `#ENVDD{id_article,type-page}`
Ajoute un filtre `dd` utilisable dans les squelettes : `[(#GET{patate}|dd)]`

Ajoute des fonctions `debug()`(pour dumper) et`d()`(pour dumper et stopper) utilisables en PHP et qui génèrent des dumps avec un habillage très lisible. Elles peuvent recevoir de multiples paramètres.
`debug_cols()` reçoit aussi plusieurs paramètres pour un afficher en colonnes côte à côte.
`debug_sql()` reçoit les même paramètres que `sql_fetsel` et affiche la requête générée.

3/ Affiche une boite d'infos (privé et public) en bas de la page : temps d'exécution de la page, nombre de requêtes Mysql exécutées, mémoire utilisée, charge serveur (load) estimée, respectivement, depuis les dernières 1, 5 et 15 minutes sur les pages html.
Ces infos sont loggées lors des appels ajax, ce qui permet de vérifier le nombre de requetes SQL générées par un calcul ajax.

Ces infos sont affichées uniquement pour les webmestres.
On peut aussi définir une IP (avec la constante `DD_INFOS_IP`) ou un tableau d'IPs (avec la globale `$GLOBALS['DD_INFOS_IP']`) depuis lesquelles ces infos seront toujours affichées (pour tester un site sans être connecté, par exemple).

3/ Un pipeline permet d'ajouter des infos dans la barre, par exemple des infos de session :

```php
function monplugin_intranet_dd_barre_infos($flux){
	$flux[] = 'Auteur : "'. $GLOBALS['visiteur_session']['id_auteur'];
	return $flux;
}
```

4/ Si `DD_INFOS_IP` est définie (pour une seule adresse IP) ou la globale `$GLOBALS['DD_INFOS_IP']` (pour en définir plusieurs dans un array), elle permet aussi de lancer un `var_profile` sans être connecté (surcharge de autoriser_debug)

5/ Pour pouvoir afficher la liste de toutes les requetes SQL exécutées sur une page en cache par exemple (sans le profiler, qui recalcule la page) en cliquant sur le total affiché, modifier le fichier `/ecrire/req/mysql` ligne 253 (SPIP 4.1) et remplacer :

```php
$r = mysqli_query($link, $query . $debug);
```
par :
```php
$start_query_time = microtime(true);
$r = mysqli_query($link, $query . $debug);
$connexion['requetes'][] = array('sql' => $query, 'time' => microtime(true) - $start_query_time);
```

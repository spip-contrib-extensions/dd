<?php

function dd_autoriser() {
}

function autoriser_dd_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('webmestre') || (defined('_DEBUG_AUTORISER') && _DEBUG_AUTORISER);
}

if (!function_exists('autoriser_debug') && !test_plugin_actif('incarner')) {
	function autoriser_debug($faire, $type, $id, $qui, $opt) {
		return autoriser('dd') || autoriser_debug_dist($faire, $type, $id, $qui, $opt);
	}
}

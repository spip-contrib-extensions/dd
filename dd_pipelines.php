<?php

function dd_formulaire_admin($flux) {
	// on modifie un peu les boutons d'admin

	// masquer les stats et le bouton "Recalculer les css", inutile avec les dernières version de scssphp
	$flux['data'] .= '<style type="text/css">#statistiques, #scssphp_calculer_css {display: none;} </style>';

	return $flux;
}

# Changelog

## Unreleased

## [1.6.0] - 2025-03-11

### Added

- Utilisation de details/summary sur le debug des tableaux (repliables)

### Modified

- Compatible SPIP 4+

## [1.5.0] - 2024-07-05

### Added

- #7 Ajouter des infobulles (title) aux infos
- Position sticky sur le thead des tables debug avec `debug_table()`
- Un dump de table avec `debug_table()`plus lisible, et qui prend en compte tablesorter s'il est installé
- Une fonction `dd_sql_error()` qui dumpe la dernière erreur SQL rencontrée et qui stoppe par défaut (die)

### Modified

- La fonction `dd()` est renommée `filtre_dd()` pour éviter une collision avec Symfony/VarDumper
- Les options de la fonction `debug_table()` sont passées dans un tableau

### Fixed

- Une parenthèse mal placée dans le test de `autoriser_dd_dist()`
- Insérer tout le temps les styles inline des .ddbox (et suppression des pipelines insert_head et header_prive)
- Refactoring des options et autorisations
- #ENVDD : retourner simplement la valeur au squelette, ne pas faire un echo

## [1.4.10] - 2023-05-05

### Added

- Compatibilité SPIP 4.2

### Modified

- $GLOBALS['_DD_INFOS_IP'] remplacée par $GLOBALS['DD_INFOS_IP'] (cohérence)
- Mise à jour de la doc (README.md)

### Fixed

- Prendre en compte une IP source forwardée par un proxy ou un waf
- Eviter 2 deprecated en PHP 8.1 (tout en restant compatible PHP 7.0)
- Affichage plus lisible de la liste des requêtes éxécutées sur une page

## [1.4.9] - 2022-05-25

### Added

- Compatibilité SPIP 4.1


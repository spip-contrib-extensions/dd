<?php

function balise_ENVDD_dist($p) {
	$args = [];
	$i = 0;
	while (($a = interprete_argument_balise(++$i, $p)) != null) {
		$args[] = $a;
	}
	if ($args) {
		$dd_args = [];
		foreach ($args as $arg) {
			$dd_args[] = '$Pile[0][' . $arg . ']';
		}
		$p->code = '_debug(' . join(',', $dd_args) . ')';
	} else {
		$p->code = '_debug($Pile[0])';
	}
	$p->interdire_scripts = false;

	return $p;
}

// à utiliser comme filtre dans un squelette : [(#ENV**|dd)]
function filtre_dd($valeur) {
	if (autoriser('dd')) {
		return _debug($valeur);
	}
	return null;
}

<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (defined('DD_INFOS_IP')) {
	$GLOBALS['DD_INFOS_IP'][] = DD_INFOS_IP;
}

// Vérifier si les ips déclarées dans $GLOBALS['DD_INFOS_IP'] sont parmi celles qui accèdent au site
if (
	isset($GLOBALS['DD_INFOS_IP'])
	&& is_array($GLOBALS['DD_INFOS_IP'])
	&& is_array($_SERVER ?? null)
) {
	$GLOBALS['DD_INFOS_IP'] = array_filter($GLOBALS['DD_INFOS_IP']);
	if (
		in_array($_SERVER['REMOTE_ADDR'] ?? null, $GLOBALS['DD_INFOS_IP'])
		|| in_array($_SERVER['HTTP_X_FORWARDED_FOR'] ?? null, $GLOBALS['DD_INFOS_IP'])
		|| in_array($_SERVER['HTTP_X_REAL_IP'] ?? null, $GLOBALS['DD_INFOS_IP'])
	) {
		if (!defined('_DEBUG_AUTORISER')) {
			define('_DEBUG_AUTORISER', true);
		}
	}
}

// ---------------------------------------------------
// Fonctions de debuggage
// ---------------------------------------------------

include_spip('inc/autoriser');

function debug() {
	if (autoriser('dd')) {
		$arg_list = func_get_args();
		foreach ($arg_list as $arg) {
			echo _debug($arg);
		}
	}
}

function debug_cols() {
	if (autoriser('dd')) {
		$arg_list = func_get_args();
		echo '<div style="display: grid; grid-gap: 0.5rem; grid-auto-columns: 1fr; grid-auto-flow: column;">';
		foreach ($arg_list as $arg) {
			echo _debug($arg);
		}
		echo '</div>';
	}
}

// debug() + die()
function d() {
	if (autoriser('dd')) {
		$arg_list = func_get_args();
		foreach ($arg_list as $arg) {
			echo _debug($arg);
		}
		die();
	}
}

function debug_sql() {
	$arg_list = func_get_args();
	$debug_items = [
		'SELECT',
		'FROM',
		'WHERE',
		'GROUP BY',
		'ORDER BY',
		'LIMIT',
		'HAVING',
	];
	$debug = '';
	foreach ($arg_list as $key => $arg) {
		if ($arg) {
			$debug .= $debug_items[$key] . ' ';
			if (is_array($arg)) {
				if ($key === 'WHERE' || $key === 2) {
					$debug .= join(' AND ', $arg);
				} else {
					$debug .= join(', ', $arg);
				}
			} else {
				$debug .= $arg;
			}
			$debug .= ' ' . "\r\n";
		}
	}
	debug(trim($debug));
}

function debug_autorise() {
	$autoriser = debug_backtrace()[1];
	if(($autoriser['args']['4']['debug'] ?? null) !== false) {
	$debug = $autoriser['function'] . '('
		. '"' . $autoriser['args']['0'].'", '
		. '"' . $autoriser['args']['1'].'", '
		. '"' . $autoriser['args']['2'].'", '
		. 'id_auteur:' . $autoriser['args']['3']['id_auteur']
		. ($autoriser['args']['4'] ? ', opt:' . json_encode($autoriser['args']['4']) : '')
		. ')';
	$autorise = $autoriser['function'](
		$autoriser['args']['0'],
		$autoriser['args']['1'],
		$autoriser['args']['2'],
		$autoriser['args']['3'],
		array_merge($autoriser['args']['4'] ?? [], ['debug' => false]),
	);
	$debug = $debug . ' ' . ($autorise ? 'YEP' : 'NOPE');
	debug($debug);
	}
}

function dd_sql_error($serveur = '', $die = true) {
	echo _debug('Error ' . sql_errno($serveur) . ' - ' . sql_error($serveur) . "\n\n" . sql_error_backtrace() . "\n\n" . trim(spip_connect($serveur)['last']), true, true);
	if ($die) {
		die();
	}
}

function debug_table($data = [], $options = []) {
	if (empty($data)) {
		return '';
	}
	static $assets;
	$couper = $options['couper'] ?? 0;
	$index_column = $options['index'] ?? false;

	if (!$assets) {
		$out = <<<EOCSS
		<style>
				.dd_table {
					border-collapse: collapse;
					border: 1px solid lightgray;
					width: 100vw;
					position: relative;
					left: 50%;
					right: 50%;
					margin-left: -50vw;
					margin-right: -50vw;
				}
				.dd_table td,
				.dd_table th {
					padding: 0.35em;
					font-size: 0.85em;
					border-collapse: collapse;
					border: 1px solid lightgray;
				}
				.dd_table_row {
					color: gray;
				}
		</style>
		EOCSS;

		include_spip('inc/utils');
		if (test_plugin_actif('tablesorter')) {
			$out = <<<EOJS
			<script>
				$(function() {
					$(function() {
						$(".dd_table").tablesorter(
							{
								theme: 'spip',
								initialized: function(table) {
									$(table).find('.tablesorter-header-inner').wrapInner('<div class="header-title">');
								}
							}
						);
					});
				});
			</script>
		EOJS;
		}
		$assets = true;
	}

	$headers = array_keys($data[0]);
	$out = '<table class="dd_table">';
	$out .= '<thead style="position: sticky; top: 0; z-index: 1;">';
	$out .= '<tr>';
	if ($index_column) {
		$out .= '<th>row</th>';
	}
	foreach ($headers as $header) {
		$out .= '<th>' . $header . '</th>';
	}
	$out .= '</tr>';
	$out .= '</thead>';
	$out .= '<tbody>';
	foreach ($data as $index => $tab2) {
		$out .= '<tr>';
		if ($index_column) {
			$out .= '<td class="dd_table_row">' . $index . '</td>';
		}
		foreach ($tab2 as $val) {
			$out .= '<td>' . ($val == '' ? '&nbsp;' : ($couper ? couper($val, $couper) : $val)) . '</td>';
		}
		$out .= '</tr>';
	}
	$out .= '</tbody>';
	$out .= '</table>';

	return $out;
}

// ---------------------------------------------------
// Fonctionq "privées" (à usage interne)
// ---------------------------------------------------

function _debug($var, $show_html = true, $simple = false) {
	$out = '';
	$scope = false;
	$prefix = 'unique';
	$suffix = 'value';

	if ($scope) {
		$vals = $scope;
	} else {
		$vals = $GLOBALS;
	}

	$new = $prefix . rand() . $suffix;
	$vname = false;
	foreach ($vals as $key => $val) {
		if ($val === $new) {
			$vname = $key;
		}
	}

	$style = '
		display: block;
		position: relative;
		margin: 0 0 1px;
		padding: 0.4em;
		border-radius: 4px;
		background: #444444;
		font-family: Consolas, "Andale Mono WT", "Andale Mono", "Lucida Console", "Lucida Sans Typewriter", "DejaVu Sans Mono", "Bitstream Vera Sans Mono", "Liberation Mono", "Nimbus Mono L", Monaco, "Courier New", Courier, monospace;
		color: white;
		font-size: 0.85rem;
		line-height: 1.5;
		text-align: left;
		white-space: pre-wrap;
		word-break: break-word;
	';
	$style = str_replace(["\n", "\t"], '', $style);

	$out .= "<div style='$style'>";
	if ($simple) {
		$out .= $var;
	} else {
		$out .= _do_dump($var, $vname, null, $show_html);
	}
	$out .= "</div>";

	return $out;
}

function _do_dump(&$var, $var_name = null, $reference = null, $show_html = true) {
	$out = '';
	$reference = $reference . $var_name;
	$keyvar = 'the_do_dump_recursion_protection_scheme';
	$keyname = 'referenced_object_name';

	if (is_array($var) && isset($var[$keyvar])) {
		$real_var = &$var[$keyvar];
		$real_name = &$var[$keyname];
		$type = ucfirst(gettype($real_var));
		$out .= "$var_name <span style='color:#e5e5e5'>$type</span> = <span style='color:#e87800;'>&amp;$real_name</span><br>";
	} else {
		$var = [
			$keyvar  => $var,
			$keyname => $reference,
		];
		$avar = &$var[$keyvar];

		$type = ucfirst(gettype($avar));
		if ($type == "String") {
			$type_color = "<span style='color:orange'>";
		} else if ($type == "Integer") {
			$type_color = "<span style='color:lightcoral'>";
		} else if ($type == "Double") {
			$type_color = "<span style='color:lightseagreen'>";
			$type = "Float";
		} else if ($type == "Boolean") {
			$type_color = "<span style='color:lightpink'>";
		} else if ($type == "NULL") {
			$type_color = "<span style='color:#ddd'>";
		}

		if (is_array($avar)) {
			$out .= '<details open>';
			$count = count($avar);
			$out .= '<summary style="cursor: pointer;">' . ($var_name ? "$var_name => " : "") . "<span style='color:#a2a2a2'>$type ($count)</span></summary>";
			$out .= '<div style="margin-left: 1rem;">';
			$keys = array_keys($avar);
			foreach ($keys as $name) {
				$value = &$avar[$name];
				$out .= _do_dump($value, "['$name']", $reference, $show_html);
			}
			$out .= '</div>';
			$out .= '</details>';
		} else if (is_object($avar)) {
			$out .= "$var_name <span style='color:#a2a2a2'>$type</span><br>(<br>";
			foreach ($avar as $name => $value) {
				$out .= _do_dump($value, "$name", $reference, $show_html);
			}
			$out .= ")<br>";
		} else if (is_int($avar)) {
			$out .= "$var_name <span style='color:#a2a2a2'>$type(" . strlen($avar) . ")</span> $type_color$avar</span><br>";
		} else if (is_string($avar)) {
			$out .= "$var_name <span style='color:#a2a2a2'>$type(" . strlen($avar) . ")</span> $type_color\"" . ($show_html ? _dd_show_html($avar) : $avar) . "\"</span><br>";
		} else if (is_float($avar)) {
			$out .= "$var_name <span style='color:#a2a2a2'>$type(" . strlen($avar) . ")</span> $type_color$avar</span><br>";
		} else if (is_bool($avar)) {
			$out .= "$var_name <span style='color:#a2a2a2'>$type(" . strlen($avar) . ")</span> $type_color" . ($avar == 1 ? "TRUE" : "FALSE") . "</span><br>";
		} else if (is_null($avar)) {
			$out .= "$var_name <span style='color:#a2a2a2'>$type</span> {$type_color}NULL</span><br>";
		} else {
			$out .= "$var_name <span style='color:#a2a2a2'>$type(" . strlen($avar) . ")</span> $avar<br>";
		}
	}

	return $out;
}

function _dd_show_html($html) {
	$html = htmlspecialchars($html);
	$replace[] = [
		'&lt;',
		'<span style="color:red;">&lt;',
	];
	$replace[] = [
		'&gt;',
		'&gt;</span>',
	];
	foreach ($replace as $r) {
		$patterns[] = '/' . str_replace('/', '\/', $r[0]) . '/';
		$replacements[] = $r[1];
	}
	$html = preg_replace($patterns, $replacements, $html);

	return nl2br($html);
}

//-----------------------------
// Affichage d'infos
//-----------------------------

function dd_affiche_infos() {
	include_spip('inc/filtres');

	// 1 - vérifier si on doit afficher les infos ou les logger

	$header_html = false;
	// tester si un content-type text/html a déjà été passé
	foreach (headers_list() as $header) {
		$header = strtolower($header);
		if (strpos($header, 'content-type') !== false && strpos($header, 'text/html') !== false) {
			$header_html = true;
		}
	}

	$r_page = _request('page') !== null ? _request('page') : '';
	$r_file = _request('file') !== null ? _request('file') : '';
	if (
		$header_html
		&& (php_sapi_name() !== 'cli')
		&& (!isset($GLOBALS['flag_preserver']) || !$GLOBALS['flag_preserver'])
		&& (!defined('_AJAX') || !_AJAX)
		&& (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || !$_SERVER['HTTP_X_REQUESTED_WITH'])
		&& !_request('var_zajax')
		&& !_request('action')
		&& !preg_match('#(\.css|\.js)#', $r_page)
		&& !preg_match('#(\.css|\.js)#', $r_file)
	) {
		$action = 'display';
	} else {
		$action = 'log';
	}

	// 2 - calculer les infos utiles

	$infos = [];
	if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
		$infos['time'] = '<span title="' . _T('dd:temps_execution_page') . '">Page : ' . number_format((microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]), 2) . ' s</span>';
	} else if (isset($GLOBALS['page_start_time'])) {
		$infos['time'] = '<span title="' . _T('dd:temps_execution_page') . '">Page : ' . number_format((microtime(true) - $GLOBALS['page_start_time']), 2) . ' s</span>';
	}

	$queries = '';
	if (isset($GLOBALS['connexions']) && is_array($GLOBALS['connexions'])) {
		foreach ($GLOBALS['connexions'] as $serveur => $connexion) {
			$info_sql = '';
			$sql_time = 0;
			if ($action === 'display' && isset($connexion['requetes']) && $connexion['requetes']) {
				$info_sql .= '<a class="dd_show_queries" data-connexion="' . $serveur . '" href="#">';
			}
			if (isset($connexion['total_requetes'])) {
				$info_sql .= '<span title="' . _T('dd:nombre_requetes_mysql') . '">' . ($serveur ? $serveur . " : " : "") . $connexion['total_requetes'] . " requêtes</span>";
			}
			if ($action === 'display' && isset($connexion['requetes']) && $connexion['requetes']) {
				$info_sql .= '</a>';
				$sql_time = 0;
				$queries = '<ol class="dd_queries" data-connexion="' . $serveur . '">';
				foreach ($connexion['requetes'] as $key => $query) {
					$sql_time += $query['time'];
					$queries .= "\n" . '<li class="dd_query" data-connexion="' . $serveur . '">'
						. '<span class="dd_query__count">' . ($key + 1) . '</span>'
						. '<span class="dd_query__sql">' . $query['sql'] . '</span>'
						. '<span class="dd_query__time">' . number_format($query['time'], 6) . '</span>'
						. '</li>';
				}
				$queries .= '</ol>';
			}
			$infos['mysql_' . $serveur] = $info_sql;
			if ($sql_time) {
				$infos['mysql_time_' . $serveur] = 'SQL ' . number_format($sql_time, 2) . ' s';
			}
		}
	} else {
		$infos['mysql'] = "0 requetes :)";
	}

	if (function_exists('memory_get_usage')) {
		$infos['memory'] = '<span title="' . _T('dd:memoire_utilisee') . '">' . round(memory_get_usage() / 1024 / 1024, 1) . ' Mo</span>';
	}

	if (function_exists('sys_getloadavg')) {
		$load = sys_getloadavg();
		$infos['load'] = '<span title="' . _T('dd:charge_minute') . '">' . number_format($load[0], 2) . '</span>'
			. '&nbsp;•&nbsp;<span title="' . _T('dd:charge_5_minutes') . '">' . number_format($load[1], 2) . '</span>'
			. '&nbsp;•&nbsp;<span title="' . _T('dd:charge_15_minutes') . '">' . number_format($load[2], 2) . '</span>';
	}

	$infos = pipeline('dd_barre_infos', $infos);

	// 3 - afficher ou logger

	if ($action === 'display') {

		echo '<div class="debug_info">' . implode('<span class="debug_sep"></span>', $infos) . '</div>';
		echo $queries;

		echo <<<EOCSS
			<style>
			.debug_info {
				background: #3c3c3c; border: 1px solid #111; color: white; font-size: 90%; position: fixed;
				bottom: 0; right: 0; z-index: 10000; padding: 1px 0.75em; display: flex; align-items: center; }
			.debug_info a, .dd_queries a {color: white; text-decoration: underline; }
			.debug_sep { display: block; width: 2px; height: 1.6em; margin-inline: 0.5em; background: white; }
			.dd_queries {font-size: 85%; position: fixed; top: 1.5em;right: 1.5em; left: 1.5em;
				height: calc(100vh - 40px - 1.5em); overflow-y: scroll; display: none; z-index: 10001; list-style: decimal;
				padding-bottom: 1em; text-align: left; }
			.dd_query { background: #343434; color: white; padding: 0.5em 1em; display: flex; justify-content: space-between; }
			.dd_query:nth-child(odd) { background: #454545; }
			.dd_query__count { padding-right: 0.75em; }
			.dd_query__sql { word-break: break-word; flex-grow: 1;}
			.dd_query__time { margin-left: 1.5em; }
			</style>
		EOCSS;

		echo <<<EOJS
			<script>
				$(function(){
					$('.debug_info').on('mousedown', function(e){
						let orignalTarget = e.originalEvent.target;
						let classlist = orignalTarget.classList;
						if(	classlist.contains('dd_show_queries') || classlist.contains('dd_queries') || classlist.contains('dd_query')){
							// gérer un timing court sur mousedown/up, pour pouvoir sélectionner du texte
							let	down = +new Date();
							let up;
							$(this).on('mouseup', function(e) {
								up = +new Date();
								if((up - down) < 200) {
									let connexion = orignalTarget.dataset.connexion;
									let queries = $('.dd_queries[data-connexion='+connexion+']');
									if(queries.is(':visible')){
										queries.hide();
										$('body').css('overflow','auto');
									} else {
										queries.show();
										$('body').css('overflow','hidden');
									}
									e.preventDefault();
								}
							});
						} else {
							$(this).remove();
							e.preventDefault();
						}
					});
				});
			</script>
		EOJS;

		$inclure = timestamp(find_in_path('css/inclure.css'));
		echo "\n<link rel='stylesheet' href='$inclure' type='text/css' media='all' />\n";
	} else {
		spip_log($_SERVER['REQUEST_URI'], 'dd');
		spip_log(supprimer_tags(str_replace('&nbsp;', ' ', implode(' | ', $infos))), 'dd');
	}
}

if (autoriser('dd')) {
	register_shutdown_function('dd_affiche_infos');
}
